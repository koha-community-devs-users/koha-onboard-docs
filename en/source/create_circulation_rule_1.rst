.. include:: images.rst

Create a Circulation Rule
______________________________

This screen will always be displayed no matter what sample data you installed with the web installer.

Here you are creating a rule which applies the pre-defined organisational decisions of your library as to what restrictions you place on patrons borrowing items.

|create circulation rule 1|

.. note::
  Numbers are the only valid input for all input fields in this form.

1. *Library branch* dropdown box: This is the library (or libraries) you want to apply the circulation rule to. By default it is set to *All*, however you can select a singular library to apply the rule to from the dropdown box.
2. *Patron categories* dropdown box: This is the patron categories you want to apply the circulation rule to. As with *Library branch* it is set to *All* by default but more options are avaliable.
3. *Item type* dropdown box: This is the item types that you want the circulation rule to apply to. Again more options are avaliable than the default selected *All* option.
4. **Current checkouts allowed:** This is the number of items allowed from the selected library, for selected patron categories and of the selected item type.
5. **Loan period:** Number of days or hours that an item is allowed out for.
6. *Units* dropdown box: Set by default to *Days*, the unit selecting in this field is applied to the numerical values written into **Loan Period** and **Renewals Period**
7. **Renewals Allowed:** Number of times a item can be renewed.
8. **Renewals Period:** Number of days or hours that a renewal lasts for.
9. *On shelf holds allowed* dropdown box: If items can be held whilst they are on the shelf.
10. Click the *Submit* button to create the circulation rule.

For example:

|create circulation rule 1 example|
