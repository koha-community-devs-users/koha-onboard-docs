.. include:: images.rst

Create a library outcome
_____________________________

This screen will tell you if the library was created successfully.

|create library 2|

1. **Create library message:** This tells you the library was created successfully
2. **Path to create a library** This is the path from the home page to create another library or alter the settings on an existing library.
3. Click the *Add a patron category* button to go to the next step.
