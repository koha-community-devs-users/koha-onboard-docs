.. include:: images.rst

Web Installer
==============

Installer Start screen
_________________________

This is the first screen of the web installer.

|web installer start screen|

1. *Language picker* dropdown box: This is specifying the language you want Koha to be in. At present there is only one option *en* (English).
2. Click the *Next* button to load the next stage of the web installer.
