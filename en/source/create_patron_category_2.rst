.. include:: images.rst

Create a patron category outcome
____________________________________

This screen will tell you if the patron category was created successfully

|create patron category 2|

1. **Create patron category message:** This tells you if the patron category was created successfully
2. **Path to create patron category:** This is the path from the home page to create another patron category or alter the settings on an existing one.
3. Click the *Add a patron* button to go to the next step
