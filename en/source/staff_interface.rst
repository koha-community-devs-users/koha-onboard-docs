.. include:: images.rst

Staff interface
____________________


The staff interface should now appear

|staff interface|



.. seealso::
  For more information about how to use the staff interface, please visit `<https://koha-community.org/documentation/>`_
