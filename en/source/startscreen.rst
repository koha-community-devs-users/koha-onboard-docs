.. include:: images.rst



Onboarding Tool
=======================


Start Screen
_______________

To get started with the onboarding tool click the *Start setting up my Koha* button.

|startscreen|

Koha onboarding tool start screen
