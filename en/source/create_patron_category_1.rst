.. include:: images.rst

Create a patron category
_____________________________

If you did not install sample patron categories in the web installer then this screen will be displayed.

The patron category is like a jelly mould; it is used to make multiple individual patron accounts with common characteristics.


You need to input/select:

  * Category code
  * Description
  * Overdue notice required
  * Category type
  * Default privacy
  * Enrolment period: In months OR Until date


  |create patron cat 1|

  1. **Category code:** code consisting of 1 or 2 consecutive capital letters.

  2. **Description:** Sentence describing what the patron category is.

  3. *Overdue notice required* dropdown button: Set by default to 'No'. This specifies if you want the patron category to receive overdue notices.

  4. **Category type:** Set by default to 'Staff'. This makes the category created a staff member.

  5. **Default privacy:** Set by default to 'Default'. The Default privacy controls the amount of time that reading history of the patron created from the patron category are stored for.

  6. **Enrolment period:** This is the amount of time that the patrons created from this patron category are enrolled for. Either input a number of months or select a date from the interactive datepicker calendar icon.

  7. Click the *Submit* button to create the patron category.

  For example:


|create patron cat 1 ex|
