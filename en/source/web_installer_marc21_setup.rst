.. include:: images.rst

Marc21 setup
__________________________________


Marc21 Basic setup
~~~~~~~~~~~~~~~~~~~~~~
The basic setup has less control over the data values you are allowed to choose to install.

Therefore all mandatory data values that are required for using Koha as it is intended are selected by default and displayed with a bulletpoint so that the user cannot unselect them.

You may choose the data values in the *Optional* area to install.

|marc21 basic setup|


1. *Select all options* button will select all data values (mandatory and optional) to be installed, if you choose to select the *Import>>* button.

2. **Default:** All the data values in this area are mandatory and selected by default, and so are only displayed with a bulletpoint so they cannot be unselected.

3. **Other data Default:** Again all the data values are mandatory and are not editable.

4. **Coded values conforming to the Z39.71-2006 holdings statements for bibliographic items:** Not required to use Koha. A holding statement tells the user if the library has a specific item and where it is located. Selecting will install sample code values for items in the holding statements.

5. **MARC code list for relators:** Not required to use Koha. A relator is a person who was invloved in the development of the item e.g. author). Selecting this will insert sample relator records into the database.

6. **Useful patron attribute types:** Not required to use Koha. Patrons have a barcode which identifies them. By clicking this checkbox your making sure that this patron barcode is displayed on the patron summary screen.

7. **Sample patron types and categories:** Not required to use Koha. Selecting this will install sample patron categories into the database. This will mean the *Create patron category* step in the onboarding tool is skipped.

8. **Sample label and patron card data:** Not required to use Koha. Selecting this will install sample data for labels and patron cards meaning it will be easier for you to create a labels and patron cards in the future.

9. **Sample holidays: Sunday, Christmas, New Year's** Not required to use Koha. Selecting this will insert sample repeatable public holidays (e.g. Christmas) into the database.

10. **A set of default item types:** Not required for Koha. Selecting this will insert sample item type data into the database, meaning *Create Item type* step in the onboarding tool will be skipped.


11. **Sample Libraries:** Not required for Koha. Selecting this will insert sample libraries into the database, meaning *Create library* step in the onboarding tool will be skipped.

12. **Sample news items:** Not required for Koha. Selecting this will insert sample news items into the database.

13. **Sample patrons:** Not required for Koha. Patrons are the people who use the Koha Library Management System, for example: borrowers and staff. Selecting this will insert sample patrons into the database, meaning *Create patron* step in the onboarding tool will be skiped.


14. **Sample quotes:** Not required for Koha. Quotes are displayed to borrowers on the OPAC (Online Public Access Catalog) interface. Selecting this will install sample quotes.

15. **Allow access to the following servers to search and download record information:** Selecting this will give you access to the Library of Congress servers which allows you to search their catalog for marc21 records and import them into your koha instance rather than you having to manually input the information yourself.

16. Click the *Import>>* button to install all the default and selected data values.


Marc21 Advanced setup
~~~~~~~~~~~~~~~~~~~~~~~~
The advanced setup gives you far greater control over the data values that are inserted into the database. All data values (both mandatory and optional) are displayed with a checkbox (which is not selected by default) so that users can choose if they want to install that data value or not.


|marc21 advanced setup|

1. **Default MARC21 Standard Authority Types:** An authority type is a template of an authority record which stores the search fields used to search an institutions catalog for an item. This option will install sample authority types.

2. **Selected matching rules for MARC21 bibliographic records:** The MARC21 matching rules are stored in the 008 field and are used to find all select the information about specific bibliographic records using the authority type search values.

3. **Default MACR21 bibliographic framework:** Following on from the marc21 selection in the previous screen selecting this checkbox (and the *Import>>* button) will actually setup the marc21 bibliographic framework as the desired marc flavour.

4. **'FA', a 'Fast Add' minimal MARC21 framework suitable for ILL, or on the fly cataloging:** The FA marc framework lets you add an item to your institutions catalog faster than the conventional method. Select this checkbox to permit FA.

5. **Sample MARC21 bibliographic frameworks for some common types of bibliographic material:** This installs the template you will use to create items. BKS is the marc21 framework for books, this holds all the fields that you must enter into when you create a item of type books.

6. **Some basic default authorised values for library locations:** Authorised values filter user inputs and if the user input matches the authorised value then the object is created.

7. **Default Koha system authorised values:** This will control what the user does when they alter the Koha system.

8. **Default classification sources and filling rules:** This will insert several common library classification sources such as dewey decimal.


9. **Default CSV export profiles:** Using Koha you can export items; the CSV profile defines how you want to export the item(s).

10. **Defines default message transports for email and sms:** This inserts the option of contacting patrons using their email address, printed message, sms, or phone number.

11. **Some basic currencies with USA dollar as default for ACQ module:** Install US dollar, Great British Pound, Canadian Dollar and Euro currencies. Set the US dollar as the default acquisition currency.

12. **Patron attributes:** This does not install any data, and it needs to be removed.

13. **Sample frequencies for subscriptions:** For items that arrive on a regular and predictable basis for example monthly magazines installing sample frequencies is useful because you can simply select a pre-installed sample frequency when you create the item.

14. **sample notices:** This inserts sample notices into the database meaning all you have to do is customize these sample notices rather than write them from scratch.

15. **Defines default messages for the enhanced messaging configuration:** The enhanced messaging configuration lets the user choose the messages they receive from the institution. The selection of this checkbox will insert sample message types to be either allowed or forbidden by the user.

16. Defines default message transports for sending item due messages:**

17. **Sample numbering patterns for subscriptions:** Subscription items like monthly magazines have a numbering pattern to define how they fit in the sequence of the other issues. This option will install sample numbering patterns saving you having to manually insert them.

.. note::
    All the data values in the *Optional* area are the same as for the Basic setup and so have been defined above.
