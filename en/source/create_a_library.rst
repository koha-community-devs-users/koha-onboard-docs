.. include:: images.rst

Create a library
___________________
If you did not install sample libraries in the web installer then this screen will be displayed.

The library you create here should be the library you work at, because if you have not installed sample libraries then this will be the library the patron and circulation rule are assigned to.


You need to input:

  * Library code
  * Name

|create library 1|

1. **Library code:** code consisting of 3 consecutive capital letters.
2. **Name:** Official name of the library.
3. Click the *Submit* button to create a library.


For example:

|create library 1 ex|

  1. **Library code:** The regular expression that filters the acceptable inputs for library code only allows you to enter 3 consecutive capital letters.
  2. **Name:** The regular expression for this field will only accept letters and spaces.
