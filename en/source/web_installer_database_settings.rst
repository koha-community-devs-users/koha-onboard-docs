.. include:: images.rst

Database Settings
__________________________

This screen informs you of the database settings and asks you to confirm them by clicking the *Next* button. For the majority of users these settings should be correct, if not then you should contact your support provider.


|database settings|


1. **Database Settings:** Check these database settings are correct.
2. Click the *Next* button to confirm the database settings


Connection Established
~~~~~~~~~~~~~~~~~~~~~~~~~~~

After you click the *Next* button the database is created, this is conveyed through a message

|connection established|

1. **Connection established message:** This informs you that the database has been successfully created.
2. Click the *Next* button to load the next stage
