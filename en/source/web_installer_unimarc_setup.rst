.. include:: images.rst

Unimarc setup
__________________________________


Unimarc Basic setup
~~~~~~~~~~~~~~~~~~~~~~

The basic setup of unimarc is the same as that of macr21;
* You are only able to select/unselect the optional data values.
* Optional values are the same for both unimarc and marc21, therefore their definition will not be repeated, please go to `web_installer_marc21_setup` to read the definitions.

|unimarc basic setup|

1. Click the *Select all options* button to select all optional data values to be installed.

2. *Default:* As with the Basic setup for Marc21 these default mandatory data values are selected by default and cannot be unseelcted as they are displayed with a bulletpoint.

3. *Optional:* As previously stated these are the same data values as defined in the *Basic setup* of MARC21.

4. Click the *Import>>* button to install the sample data.



Unimarc Advanced setup
~~~~~~~~~~~~~~~~~~~~~~~~~~

There are only 2 data values in the Unimarc advanced setup which are unique to Unimarc:
* Authority Structure for UNIMARC in English
* Default UNIMARC bibliographic framework in English


|unimarc advanced setup|

1. Click the *Select all options* button to select all default and optional data values to be installed.

2. *Authority structure for UNIMARC in English:* Authority records store the search terms used to find items in the database. Unlike with marc21, when using unimarc the authority structure needs to be compatible with unimarc. This option makes it compatible.

3. *Default UNIMARC bibliographic framework in English:* In the previous screen you clicked to use the unimarc flavour, now by selecting this option you are confirming you want to use that framework.

4. Click the *Import>>* button to install the sample data
