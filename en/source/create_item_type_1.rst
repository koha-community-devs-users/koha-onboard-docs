.. include:: images.rst

Create a Item type
______________________

If you did not install sample item types in the web installer then this screen will be displayed.

As with the patron category the item type is basically a template which you use to make multiple items with common characteristics.

You need to input:

* Item type code
* Description

|create item type 1|

1. **Item type code:** Code consisting of 2 or 3 consecutive capital letters
2. **Description:** Sentence describing what the item type is.
3. Click the *Submit* button to create the item type

For example:

|create item type 1 example|

.. note:: The regular expression filtering Item Type code input will only accept 2 or 3 consecutive capital letters.
