.. include:: images.rst

Installer Perl Modules Installed
___________________________________

This screen tells you that the installer has all the dependencies it needs, to create the database in the next screen.


|perl_modules_installed|

Click on the *Next* button to load the next stage of the web installer.
