.. include:: images.rst

Create a Circulation Rule outcome
____________________________________

This screen will tell you if the circulation rule was created successfully.

|create circulation rule 2|

1. **Create circulation rule message:** This tells you if the circulation rule was successfully created.
2. **Path to create circulation rule:** This is the path from the home page to create another circulation rule or alter an existing one
3. Click the *Finish* button to log into the staff interface using the patron account credentials you have just made using the onboarding tool.
