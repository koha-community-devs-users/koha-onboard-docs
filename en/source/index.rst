
Koha 17.05 Installation Documentation (en)
============================================

.. include:: images.rst
.. toctree::
   :maxdepth: 2
   :numbered:

   intro
   web_installer_start_screen
   web_installer_perl_modules
   web_installer_database_settings
   web_installer_ready_for_data
   web_installer_database_tables_created
   web_installer_install_basic_configurations
   web_installer_setup_marc_flavour
   web_installer_marc21_setup
   web_installer_unimarc_setup
   selected_data_added
   redirect_to_onboarding
   startscreen
   create_a_library
   create_a_library_2
   create_patron_category_1
   create_patron_category_2
   create_patron
   create_patron_2
   create_item_type_1
   create_item_type_2
   create_circulation_rule_1
   create_circulation_rule_2
   login
   staff_interface
   license
