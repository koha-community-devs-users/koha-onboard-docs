.. include:: images.rst

Create a patron outcome
__________________________

This screen will tell you if the patron account was created successfully.

|create patron 2|

1. **Create patron message:** This tells you if the patron account was created successfully
2. **Path to create patron:** This is the path from the home page to create another patron or alter an existing one
3. Click the *Add an item type* button to go to the next step
