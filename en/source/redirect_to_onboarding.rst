.. include:: images.rst

Redirect to onboarding tool
__________________________________

If you wait between 5 and 7 second this screen should redirect you to the onboarding tool start screen.

|redirect|


If after waiting you are not directed select the link pointed out by the arrow in the above screenshot.
