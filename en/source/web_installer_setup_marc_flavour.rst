.. include:: images.rst

Select Setup and MARC flavour
__________________________________

There are two choices you need to make on this screen:


* **Setup type**- Decide on the level of control over the data that is installed into the Koha database that you want.
* **MARC flavour**- Decide on what format you want the bibliographic (catalog) records to be stored in the database in.


|select setup marc flavour|


1. **Basic setup:** Selected by default, as it is recommended for new users. You have limited control over what data values are installed. All mandatory data values are installed for you by default, you can only select a few optional data values for installation.

2. **Advanced setup:** This setup type gives you full control over what data values are installed. You can choose not to install the mandatory data values.

3. **Unimarc:** This is frequently used in the European continental countries such as Italy.

4. **Marc21:** Selected by default, as it is more commonly used globally than Unimarc.

5. Click the *Next* button to confirm your choices
