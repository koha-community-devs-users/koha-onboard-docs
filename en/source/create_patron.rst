.. include:: images.rst

Create a patron
___________________

Even if you installed sample patron data in the web installer this screen will be displayed for you to create a patron to log into the Koha staff interface with once you have finished using onboarding process.

.. note:: It is very important for you to remember the username and password you enter into this screen as it will be your unique user account which you will use on a day to day basis with the staff interface.

You will need to input/select:

* Surname
* First Name
* Card number
* Library
* Patron category
* Username
* Password

|create patron 1|

.. note:: All inputs on this form wil only accept inputs consisting of letters, numbers, and spaces.

1. **Surname:** Surname (last name) in full.
2. **First name:** First name (christian name) in full. Middle name(s) are not required.
3. **Card Number:** A number to the right of this input box will indicate the value to input into this field, as this number must be unique to you alone.
4. *Library* dropdown box: If you created a library using this onboarding tool, this will be the only option. Otherwise choose a random library from the sample libraries you installed in the web installer.
5. *Patron category* dropdown box: If you created a patron category using this onboarding tool, this will be the only option. Otherwise choose the *Staff* patron category.
6. **Superlibrarian permission:** This non-editable setting will allow you to complete all librarian tasks (without restrictions) when you log into the staff interface.
7. **Username:** The username you will log into the staff and user interface (OPAC) with.
8. **Password:** A password consisting of letters, numbers, and spaces only which is greater than 8 characters.
9. **Confirm password:** Repeat the above password again
10. Click the *Submit* button to create the patron account

For example:

|create patron 1 ex|

Common errors creating patrons
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. warning::
  After clicking on the *Submit* button you may find that the following errors are displayed.

  |create patron error|

  1. **Cardnumber already in use:** This means that someone already has the card number that you entered. Cardnumbers are unique values, so you need to enter a higher card number value.
  2. **Passwords do not match:** The passwords you wrote into the **Password** and **Confirm password** fields do not match.

  Just click the *Try again* button and fill out the form again making sure to enter a higher number for the **Card number** and the identical password in both the **Password** and **Confirm Password** fields.
