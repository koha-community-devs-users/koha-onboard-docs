.. include:: images.rst

Login to access staff interface
_________________________________

You have now finished using the onboarding tool and can log into the staff interface using the patron account credentials you created in the onboarding tool

|login|

1. **Username:** Enter the username you created for the patron
2. **Password:** Enter the password you created
3. **Library:** This is the library staff interface you want to log into. The options are either: *My library* or the library you installed/created. Leaving the default selected option of *My library* is fine to log in with first time.
4. Click the *Login* button to access the staff interface

For example:


|login example|
