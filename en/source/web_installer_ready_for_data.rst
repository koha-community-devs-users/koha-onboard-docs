.. include:: images.rst

Ready to fill tables with data
____________________________________

This screen is informing you everything is set up for you to create the database tables in the next screen.

|ready for data|

Click the *Next* button to load the next stage
